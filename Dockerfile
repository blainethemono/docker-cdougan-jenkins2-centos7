FROM openshift/jenkins-2-centos7

USER root
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum install -y \
      docker-client \
      wget \
      https://packages.chef.io/files/stable/inspec/1.48.0/el/7/inspec-1.48.0-1.el7.x86_64.rpm \
      gcc \
      make \
      curl-devel \
      expat-devel \
      gettext-devel \
      openssl-devel \
      zlib-devel \
      zlib-devel perl-ExtUtils-MakeMaker \
      ansible \
      ansible-lint \
      ansible-review \
      standard-test-roles \
      rpm-build \
      python2-ansible-tower-cli \
      jq \
      python2-jenkins-job-builder \
      PyYAML \
      zlib \
      gcc-c++ \
      patch \
      readline \
      readline-devel \
      libyaml-devel \
      libffi-devel \
      bzip2 \
      autoconf \
      automake \
      libtool \
      bison \
      curl \
      sqlite-devel

USER root
RUN cd /usr/src && \
      wget https://www.kernel.org/pub/software/scm/git/git-2.9.5.tar.gz && \
      tar xvf git-2.9.5.tar.gz && \
      cd git-2.9.5 && \
      make prefix=/usr/local/git all && \
      make prefix=/usr/local/git install && \
      echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc && \
      source /etc/bashrc && \
      git --version && \
      rm -f /usr/src/git-2.9.5.tar.gz && \
      rm -rf /usr/src/git-2.9.5

USER jenkins
RUN   cd $HOME && \
      git clone git://github.com/sstephenson/rbenv.git .rbenv && \
      git clone https://github.com/rbenv/ruby-build.git $HOME/.rbenv/plugins/ruby-build && \
      echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.bash_profile && \
      echo 'eval "$(rbenv init -)"' >> $HOME/.bash_profile && \
      source $HOME/.bash_profile && \
      rbenv install -v 2.4.3 && \
      rbenv global 2.4.3 && \
      ruby -v && \
    gem install \
      puppet:3.8.7 \
      rspec-puppet \
      serverspec \
      rake \
      puppet-lint \
      ansible_spec_plus

USER root
RUN curl -fsSL https://goss.rocks/install | sh
